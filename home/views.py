from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from loginmanager.models import *

# Create your views here.

@login_required(login_url='loginmanager-login')
def home(request):
    try:
        user = request.user
        user_profile = Profile.objects.get(user=user)
        return redirect("setup_eye")
    except:
        return redirect("profile")
    return render(request, 'home/index.html')


