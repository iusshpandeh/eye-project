from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static



urlpatterns = [
    path('', views.loginpage, name='loginmanager-login'),
    path('register/', views.registerpage, name='loginmanager-register'),
    path('profile/', views.profile, name='profile'),
    path('logout', views.logoutpage, name='logout'),

    path(
        "password-reset/",
        auth_views.PasswordResetView.as_view(
            template_name="loginmanager/password_reset.html"),
        name="password_reset",
    ),
    path(
        "password-reset/done/",
        auth_views.PasswordResetDoneView.as_view(
            template_name="loginmanager/password_reset_done.html"),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="loginmanager/password_reset_confirm.html"),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="loginmanager/password_reset_complete.html"),
        name="password_reset_complete",
    ),
    
    path("setup_eye",views.setup_eye, name="setup_eye"),
    path("upload_image/",views.upload_image, name = "upload_image"),
    
    
    
    path("profile_not_setup", views.profile_not_setup, name="profile_not_setup"),
    path("red_eyes", views.red_eyes, name = "red_eye"),
    path("drowsy", views.drowsy, name = "drowsy"),
    path("is_ok", views.is_ok, name = "is_ok"),
    #path("upset", views.upset, name="upset"),
    path("setup_face_recog",views.setup_face_recog, name="setup_face_recog"),
    path("upload_image/",views.upload_image, name = "upload_image"),
    path("identify_user/", views.identify_user, name="identify_user"),
    path("face_recog_login", views.face_recog_login, name = "face_recog_login"),
    path("del_face_recog_data", views.del_face_recog_data, name="del_face_recog_data"),
    path("test_eye", views.test_eye, name="test_eye"),
    path("register_image/", views.register_image, name="register_image"),
    # path("download_data", views.download_data,name = "download_data"),
    # path("download_report/<str:uname>/", views.download_report, name="download_report"),
    # path("download_logs/<str:uname>/", views.download_logs, name="download_logs"),
    path("upload_custom_image", views.upload_custom_image, name="upload_custom_image"),
    path("compute_results", views.compute_results, name="compute_results"),
    path("display_results", views.display_results, name="display_results"),
    path("bulk_upload", views.bulk_upload, name="bulk_upload"),
    path("upload_user_image/<str:uname>", views.upload_user_image, name = "upload_user_image")
    
] + static('/models', document_root='loginmanager/static/loginmanager/js/models')
