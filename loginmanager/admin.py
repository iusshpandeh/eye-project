from django.contrib import admin

# Register your models here.
from .models import Profile, Log, Report

from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter

admin.site.register(Profile)

from django.contrib.admin import DateFieldListFilter


import csv
from django.http import HttpResponse

def export_as_csv_action(description="Export selected objects as CSV file", fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])

        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset

        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % str(opts).replace('.', '_')

        writer = csv.writer(response)

        if header:
            writer.writerow(list(field_names))
        for obj in queryset:
            writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = description
    return export_as_csv


class ReportAdmin(admin.ModelAdmin):
    actions = [export_as_csv_action("CSV Export")]
    list_display = ["person","record_time","final_pred"]
    search_fields = ["person__username","person__email" ,"final_pred"]
    list_filter = (
        ('record_time', DateRangeFilter),
        ('record_time',DateFieldListFilter),
        "person",
        "final_pred"
    )

class LogAdmin(admin.ModelAdmin):
    actions = [export_as_csv_action("CSV Export")]
    list_display = ["person","record_time"]
    list_filter = [
        ('record_time', DateRangeFilter), 
        "person"
        
    ]

admin.site.register(Report, ReportAdmin)
admin.site.register(Log, LogAdmin)