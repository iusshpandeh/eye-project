const video = document.getElementById('video');
const para = document.getElementById('para')
const expr = document.getElementById('expr')
const fd = document.getElementById('fd')
var count=0;

var is_ok = 0
var is_drowsy = 0
var is_red = 0


var complete = 0

negative_emotions = ["angry", "disgusted", "sad", "fearful"]

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/login/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/login/models')
]).then(startVideo2).catch(e=>console.error('An Error Occured'));


function startVideo2() {
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
    console.log("getUserMedia() not supported.");
    return;
  }
  const params = {
    video: {
        width: 640,
        height: 480,
        facingMode: "user"
    }
  }

  navigator.mediaDevices.getUserMedia(params)
  .then(function(stream) {
      var video = document.querySelector('video');
      if ("srcObject" in video) {
          video.srcObject = stream;
      } else {
          video.src = window.URL.createObjectURL(stream);
      }
      video.onloadedmetadata = function(e) {
          video.play();
      };
  })
  .catch(function(err) {
        console.log(err.name + ": " + err.message);
    });

}

function stopVideo(){
  video.pause();
  video.currentTime = 0;
}

video.addEventListener('playing', detect)
var upload_count = 0
var hit_count = 0
var not_found = 0
var root_url = window.location.origin;


function detect() {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  
  setInterval(async () => {
    const detections = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions()).withFaceExpressions()
    console.log(detections)
    
    detection = detections['detection']
    expression = detections['expressions']
    console.log(expression)
    if (detections.length != 0) {
      score = detection["_score"]
      if (score > 0.60){
        update_fd("Face Detected with/顔を検出しました " + score + " Confidence")
        exp = Object.keys(expression).reduce(function(a, b){ return expression[a] > expression[b] ? a : b });
            // if (negative_emotions.includes(exp)){
            //   is_upset = is_upset + 1
            // }
        update_exp("Facial Expression/顔の表情: " + exp)
            if (complete == 0){
              takeASnap(exp)
              
            }
            

            
          
        
        
        
    } 
    else{
      not_found = not_found + 1

      if (not_found > 200){
        
        stopVideo()
        video.removeEventListener('playing',detect)
        window.alert("Server Busy. Try Again Later.")        
      }

    }
      }
      

  

    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    //faceapi.draw.drawDetections(canvas, resizedDetections)
  }, 50)
}

function update_logs(text){
  para.innerHTML = text;
  para.style.visibility = "visible";

}

function update_exp(text){
  expr.innerHTML = text;
  expr.style.visibility = "visible";
}

function update_fd(text){
  fd.innerHTML = text;
  fd.style.visibility = "visible";
}
function takeASnap(exp){
  const canvas2 = document.createElement('canvas'); // create a canvas
  const ctx = canvas2.getContext('2d'); // get its context
  canvas2.width = video.videoWidth; // set its size to the one of the video
  canvas2.height = video.videoHeight;
  ctx.drawImage(video, 0,0); // the video
  var img = new Image();
  img.src = canvas2.toDataURL("image/jpg");
  snapshot = document.getElementById("snapshot")
  snapshot.innerHTML = '';
  snapshot.appendChild(img)
  console.log("imageTaken")
  uploadImage(exp)
}



function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var arrayBuffer = new ArrayBuffer(byteString.length);
  var _ia = new Uint8Array(arrayBuffer);
  for (var i = 0; i < byteString.length; i++) {
      _ia[i] = byteString.charCodeAt(i);
  }

  var dataView = new DataView(arrayBuffer);
  var blob = new Blob([dataView], { type: mimeString });
  return blob;
}

var root_url = window.location.origin;



function uploadImage(exp){
  var url = 'upload_image/';
  var fdata = new FormData();
  var dataURI = snapshot.firstChild.getAttribute("src");
  var imageData = dataURItoBlob(dataURI);
  fdata.append("image", imageData, "frimage.jpg");
  fdata.append("expression", exp)

  var root_url = window.location.origin;
  console.log(root_url)

  let req = new Request(url,{
    method:'POST',
    mode: 'cors',
    body:fdata
  });
  
  fetch(req).then( (response)=> {
          // if(response.status == 201){
          //   is_ok = is_ok + 1
          //   update_logs("Frame Status: OKAY")
          //   if (is_ok >= 8) {
          //     if (complete != 1){
          //       complete = 1
          //       window.location.href = root_url + "/login/is_ok"
          //     }
          //   }
          //   console.log("Image Uploaded")
          //   }
          // else if (response.status == 202){
          //   is_red = is_red + 1
          //   update_logs("Frame Status: RED EYES")
          //   if (is_red >= 2){
          //     if (complete != 1) {
          //     complete = 1
          //     window.location.href = root_url + "/login/red_eyes"
          //   }}
          //   console.log("Failed to upload")

            

          // }

          // else if (response.status == 203){
          //   console.log("Drowsy Recieved")
          //   update_logs("Frame Status: DROWSY EYES")
          //   is_drowsy = is_drowsy + 1
          //   console.log("Drowsy count")
          //   console.log(is_drowsy)
          //   if (is_drowsy >= 5){
          //     console.log("DROWSY")
          //     if (complete != 1) {
          //     complete = 1
          //     window.location.href = root_url + "/login/drowsy" }
          //   }
        if (response.status == 202){
          console.log("Image Uploaded")
        }
        else if (response.status == 203){
          complete = 1

          console.log("Upload complete")
          location = location
        }
          
        

          

          else {
            console.log("Skipped")
          }
          
          //check_ok()



          }
    


  )
  .catch( (err) => {
    console.log(err.message)
  });


  }

