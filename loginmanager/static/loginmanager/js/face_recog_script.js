const video = document.getElementById('video');

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/login/models')
]).then(startVideo2).catch(e=>console.error('An Error Occured'));


function startVideo2() {
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
    console.log("getUserMedia() not supported.");
    return;
  }
  const params = {
    video: {
        width: 640,
        height: 480,
        facingMode: "user"
    }
  }

  navigator.mediaDevices.getUserMedia(params)
  .then(function(stream) {
      var video = document.querySelector('video');
      if ("srcObject" in video) {
          video.srcObject = stream;
      } else {
          video.src = window.URL.createObjectURL(stream);
      }
      video.onloadedmetadata = function(e) {
          video.play();
      };
  })
  .catch(function(err) {
        console.log(err.name + ": " + err.message);
    });

}

video.addEventListener('playing', detect)

var flag = 0

function detect() {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
    if (detections.length != 0) {
      score = detections[0]["_score"]
      if (score > 0.85){
        resp = takeASnap()
        if (resp==1){
          console.log("Image uploaded")
        }
      }

  }

    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
  }, 200)
}


var upload_count = 0
var recog_flag = 0

function takeASnap(){
  const canvas2 = document.createElement('canvas'); // create a canvas
  const ctx = canvas2.getContext('2d'); // get its context
  canvas2.width = video.videoWidth; // set its size to the one of the video
  canvas2.height = video.videoHeight;
  ctx.drawImage(video, 0,0); // the video
  var img = new Image();
  img.src = canvas2.toDataURL("image/jpg");
  snapshot = document.getElementById("snapshot")
  snapshot.innerHTML = '';
  snapshot.appendChild(img)
  console.log("imageTaken")
  uploadImage()
  
}

function stopVideo(){
  video.pause();
  video.currentTime = 0;
}

function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var arrayBuffer = new ArrayBuffer(byteString.length);
  var _ia = new Uint8Array(arrayBuffer);
  for (var i = 0; i < byteString.length; i++) {
      _ia[i] = byteString.charCodeAt(i);
  }

  var dataView = new DataView(arrayBuffer);
  var blob = new Blob([dataView], { type: mimeString });
  return blob;
}


function uploadImage(){
  var url = 'identify_user/';
  var fdata = new FormData();
  var dataURI = snapshot.firstChild.getAttribute("src");
  var imageData = dataURItoBlob(dataURI);
  fdata.append("image", imageData, "frimage.jpg");

  var root_url = window.location.origin;
  console.log(root_url)

  let req = new Request(url,{
    method:'POST',
    mode: 'cors',
    body:fdata
  });
  console.log("not recoged yet", recog_flag)
  if (recog_flag == 0) {
    
    if (upload_count < 3){
      fetch(req).then( (response)=> {
        flag = 1
        if(response.status == 202){
          stopVideo()
          video.removeEventListener('playing',detect)
          recog_flag = 1
          location = root_url + "/login/test_eye"
          
              }
        else {
            upload_count = upload_count + 1
            console.log("no one indentified")
        }
          }
    
  )
  .catch( (err) => {
    console.log(err.message)
  });
  
  
    }

  if(upload_count == 10){
    video.removeEventListener('playing',detect)
    window.alert("Failed to Identify ! 認識できません !!")
    window.location.href = root_url + '/';
  }
  }
  
}
