const video = document.getElementById('video');
var count=0;


Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/login/models')
]).then(startVideo2).catch(e=>console.error('An Error Occured'));


function startVideo2() {
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
    console.log("getUserMedia() not supported.");
    return;
  }
  const params = {
    video: {
        width: 640,
        height: 480,
        facingMode: "user"
    }
  }

  navigator.mediaDevices.getUserMedia(params)
  .then(function(stream) {
      var video = document.querySelector('video');
      if ("srcObject" in video) {
          video.srcObject = stream;
      } else {
          video.src = window.URL.createObjectURL(stream);
      }
      video.onloadedmetadata = function(e) {
          video.play();
      };
  })
  .catch(function(err) {
        console.log(err.name + ": " + err.message);
    });

}

function stopVideo(){
  video.pause();
  video.currentTime = 0;
}

video.addEventListener('playing', detect)
var upload_count = 0
var hit_count = 0
var not_found = 0
var root_url = window.location.origin;


function detect() {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  
  setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
    if (detections.length != 0) {
      score = detections[0]["_score"]
      if (score > 0.85){
        if (hit_count < 10){
          hit_count = hit_count + 1
          if (upload_count < 5){
            upload_count = upload_count + 1
            takeASnap()

            }
          
          else{
            stopVideo()
            video.removeEventListener('playing', detect)
            //window.location.href = root_url + "/login/profile/";
            window.location.replace(root_url)
          }
        }
        
        
    } 
    else{
      not_found = not_found + 1

      if (not_found > 50){
        
        stopVideo()
        video.removeEventListener('playing',detect)
        window.alert("FAILED. Try Again.")
        //window.location.href = root_url + "/login/profile/";
        
        
      }

    }
      }
      

  

    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
  }, 500)
}



function takeASnap(){
  const canvas2 = document.createElement('canvas'); // create a canvas
  const ctx = canvas2.getContext('2d'); // get its context
  canvas2.width = video.videoWidth; // set its size to the one of the video
  canvas2.height = video.videoHeight;
  ctx.drawImage(video, 0,0); // the video
  var img = new Image();
  img.src = canvas2.toDataURL("image/jpg");
  snapshot = document.getElementById("snapshot")
  snapshot.innerHTML = '';
  snapshot.appendChild(img)
  console.log("imageTaken")
  uploadImage()
}



function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var arrayBuffer = new ArrayBuffer(byteString.length);
  var _ia = new Uint8Array(arrayBuffer);
  for (var i = 0; i < byteString.length; i++) {
      _ia[i] = byteString.charCodeAt(i);
  }

  var dataView = new DataView(arrayBuffer);
  var blob = new Blob([dataView], { type: mimeString });
  return blob;
}

var root_url = window.location.origin;

function uploadImage(){
  var url = 'register_image/';
  var fdata = new FormData();
  var dataURI = snapshot.firstChild.getAttribute("src");
  var imageData = dataURItoBlob(dataURI);
  fdata.append("image", imageData, "frimage.jpg");

  var root_url = window.location.origin;
  console.log(root_url)

  let req = new Request(url,{
    method:'POST',
    mode: 'cors',
    body:fdata
  });
  
  fetch(req).then( (response)=> {
          if(response.status == 201){
            console.log("Image Uploaded")
            }
          else{
            
            console.log("Failed to upload")

          }
            
          }
    


  )
  .catch( (err) => {
    console.log(err.message)
  });


  }

