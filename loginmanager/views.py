from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.contrib import messages
from .decorators import unauthenticated_user, admin_only
from .forms import CreateUserForm, ProfileForm, UploadFileForm
from django.contrib.auth.decorators import login_required
from .models import *
from django.contrib.auth.decorators import user_passes_test
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import datetime
from django.conf import settings
from django.utils import timezone
import face_recognition
import cv2
import psycopg2
import os
from skimage import exposure
import numpy as np
import pandas as pd
import calendar
from django.contrib.auth import logout
from numpy.linalg import norm
from django.utils import timezone

# Create your views here.

@unauthenticated_user
def loginpage(request):
    # if request.user.is_authenticated:
    #     return redirect('home')
    print(request)
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        print(user)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'Wrong Username or Password')

    context = {}
    return render(request, 'loginmanager/login.html', context)
    # return HttpResponse('Login home page')

def logoutpage(request):
    logout(request)
    return redirect('home')

@unauthenticated_user
def registerpage(request):
    # if request.user.is_authenticated:
    #     return redirect('home')

    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='User')
            user.groups.add(group)

            messages.success(request, 'Account was Created for ' + username)
            return redirect('home')

    context = {'form': form}
    return render(request, 'loginmanager/register.html', context)



def profile(request):
    if not request.user.is_authenticated:
        return redirect("loginmanager-login")
    user = request.user
    print(user)
    Profilefound = None
    try:
        Profilefound = request.user.profile.user
    except:
        pass

    print(Profilefound)
    if Profilefound is None:
        # print('No profile found')
        form = ProfileForm(instance=user)
    else:
        # print('Profile found')
        profile = request.user.profile
        form = ProfileForm(instance=profile)

    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if Profilefound == None:
            print("First setup")
            if form.is_valid():
                print("Valid Form")
                profiledata = form.save(commit=False)
                profiledata.user = request.user
                # print('form is valid')
                profiledata.save()
                # form.save()
                messages.success(request, 'Profile Created Successfully ')
        else:
            form = ProfileForm(request.POST, instance=profile)
            if form.is_valid():
                form.save()
                messages.success(request, 'Profile Updated Successfully ')


    context = {'form':form}
    return render(request, 'loginmanager/profile.html', context)


def profile_not_setup(request):
    return render(request,"loginmanager/profile_not_setup.html")

from django.views.static import serve

def download_data(request):
    users = Profile.objects.all()
    context = {"all_users":users}
    return render(request, "loginmanager/download_data.html", context)

def bulk_upload(request):
    try:
        users = Profile.objects.filter(lear__isnull = True)
    except:
        messages.warning(request, "No Profile have images left to upload")
        users = None
    
    context = {"all_users":users}
    return render(request, "loginmanager/bulk_upload.html", context)

def upload_user_image(request, uname):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        user = uname
        if form.is_valid():
            resp = upload_face(request.FILES['file'], user)
            if resp == 1:
                messages.success(request, "Image upload successful")
                
            else:
                messages.warning(request, "ERROR!!! Image upload FAILED. Use another image")
            return redirect("upload_user_image", uname = uname)
        print(user)
    else:
        form = UploadFileForm

    return render(request,"loginmanager/upload_user_image.html", {'form':form})


def download_report(request, uname):
    try:
        filepath = 'reports/' + str(uname) + ".csv"
        return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    except:
        return HttpResponse("404. The requested file might not exist in the server. ")

import shutil
def download_logs(request,uname):
    filepath = 'logs/' + uname
    try:
        os.remove(filepath + ".zip")
    except FileNotFoundError as e:
        pass
    try:
        shutil.make_archive(filepath, "zip", filepath + "/")
    except:
        return HttpResponse("404. The requested file might not exist in the server. ")
    return serve(request, os.path.basename(filepath + ".zip"), os.path.dirname(filepath))



def setup_eye(request):
    if request.method == 'POST':
        if "proceed" in request.POST:
            return redirect("test_eye")
            

        if "download_fr" in request.POST:
            filepath = 'reports/' + str(request.user) + ".csv"
            return serve(request, os.path.basename(filepath), os.path.dirname(filepath))

    try:
        profile = Profile.objects.get(user = request.user)
    except:
        return redirect("loginmanager-login")
    lear = profile.lear

    if lear:
        return render(request, "loginmanager/setup_eye.html")
    else:
        messages.warning(request, "Face Recognition MUST be done before proceeding with eye test. 顔表情判定を行うまえに、顔認証を行う必要があります。 ")
        return redirect("setup_face_recog")
   

def test_eye(request):
    if request.user.is_authenticated:
        try:
            if "eye_complete" not in request.session:
                user = request.user
                user_profile = Profile.objects.get(user=user)
                context = {'profile':user_profile}
                return render(request, "loginmanager/capture_image.html",context)
            else:
                return redirect("display_results")
        except:
            return HttpResponse("PROFILE NOT SET UP! Please set up your profile FIRST!")
        
    else:
        return render(request, "loginmanager/face_recog_login.html")

def display_results(request):
    return redirect("compute_results")


def compute_results(request):
    logs = Log.objects.filter(person = request.user, reported
     = False,record_time__gte=timezone.now() - timezone.timedelta(seconds=90))
    
    if logs.count() == 0:
        return HttpResponse("SERVER FAILURE. CONTACT ADMIN")
    lear =[]
    rear = []
    fr = []
    fe = []
    er = []
    exp = []
    for log in logs:
        lear.append(log.left_ear)
        rear.append(log.right_ear)
        fr.append(log.redness_face)
        fe.append(log.exposure_face)
        er.append(log.redness_eye)
        exp.append(log.expression)
    try:
        profile = Profile.objects.get(user=request.user)
        lear_thresh = profile.lear * 0.6
        rear_thresh = profile.rear * 0.6
    except:
        return HttpResponse("Please Register your face again. This is due to System Upgrade")
    lblinks = sum([1 if l < lear_thresh else 0 for l in lear])
    rblinks = sum([1 if l < rear_thresh else 0 for l in rear])
    lblink_ratio = lblinks/len(lear)
    rblink_ratio = rblinks/len(rear)

    if lblink_ratio + rblink_ratio >= 0.5:
        drowsy = True
    else:
        drowsy = False
    
    expression = max(set(exp), key=exp.count)

    positive_emotions = ["happy", "neutral", "surprised"]
    negative_emotions = ["angry", "disgusted", "sad", "fearful"]

    if expression in positive_emotions:
        positive_expression = True

    eye_redness = max(er)
    if eye_redness >= 90:
        red_eye = True
    else:
        red_eye = False

    face_redness = max(fr)
    if face_redness > 50:
        red_face = True
    else:
        red_face = False

    if drowsy & red_eye & red_face:
        status = "Tired and Sick/病気/疲れた"
    
    elif drowsy & red_eye:
        status = "Tired and Sleepy/疲れた/眠気"

    elif drowsy:
        status = "Drowsy/眠気"

    elif not positive_expression:
        status = "Sad/悲しい"

    else:
        status = "Fine/良好"
    

    report = Report(person = request.user, final_pred = status, lear_avg = sum(lear) / len(lear), rear_avg = sum(rear)/len(rear), eye_redness_max = eye_redness,
                    face_exposure_avg = sum(fe)/len(fe), face_redness_avg = sum(fr)/len(fr), expression = expression, lblinks = lblinks, rblinks = rblinks)

    curr_report = Report.objects.filter(person=request.user).last()

    if curr_report:
        diff = timezone.now() - curr_report.record_time
        diff = diff.total_seconds()
        if diff < 30:
            pass
        else:
            report.save()
    else:
        report.save()

    curr_report = Report.objects.filter(person=request.user).last()

    logout(request)

    
    #logs.update(reported = True)
    return render(request
    ,"loginmanager/display_data.html", {"data":curr_report, "frames":logs.count()})

import json 
import csv





def drowsy(request):
    if request.user.is_authenticated: 
        user = request.user
        filename = "reports/" + str(user) + ".csv"
        file_exists = os.path.isfile(filename)
        log = request.session.get("log_id")
        if log:
            with open(filename, "a") as f:
                    headers = ['user','date','status','log file', 'max red','left eye', 'right eye','est blinks' ,'expression', "face_redness","face_brightness"]
                    file_is_empty = os.stat(filename).st_size == 0
                    writer = csv.writer(f)
                    data = pd.read_csv(log)
                    data.columns = ["redness","face_redness","face_brightness", "left eye data", "right eye data", "expression captured"]
                    avg_left = data["left eye data"].mean()
                    avg_right = data["right eye data"].mean()
                    exp = data["expression captured"].value_counts().index[0]
                    max_red = data["redness"].max()
                    est_blinks = data[data["left eye data"] < 0.20].shape[0] * 2
                    avg_fr = data["face_redness"].max()
                    avg_fb = data["face_brightness"].max()
                    print("Est blinks", est_blinks)
                    if file_is_empty:
                        writer.writerow(headers) 
                    write_data = [user, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Drowsy", str(log), max_red, avg_left, avg_right,est_blinks, exp,avg_fr,avg_fb ]
                    writer.writerow(write_data)
                    data_html = data.to_html()
                    context = {"status": "Drowsy and Sleepy", "lear":avg_left, "rear":avg_right, "er":max_red,
                                "tfc":len(data.index) * 5, "eb":est_blinks, "ec":exp,"fb":avg_fb, "fr":avg_fr}

        logout(request)
        return render(request,"loginmanager/drowsy.html", context)
    else:
        return redirect("/")

def is_ok(request):
    if request.user.is_authenticated: 
        user = request.user
        filename = "reports/" + str(user) + ".csv"
        file_exists = os.path.isfile(filename)
        log = request.session.get("log_id")
        print(log)
        if log:
             with open(filename, "a") as f:
                headers = ['user','date','status','log file', 'max red','left eye', 'right eye','est blinks' ,'expression', "face_redness","face_brightness"]
                file_is_empty = os.stat(filename).st_size == 0
                writer = csv.writer(f)
                data = pd.read_csv(log)
                data.columns = ["redness","face_redness","face_brightness", "left eye data", "right eye data", "expression captured"]
                avg_left = data["left eye data"].mean()
                avg_right = data["right eye data"].mean()
                exp = data["expression captured"].value_counts().index[0]
                max_red = data["redness"].max()
                est_blinks = (data[data["left eye data"] < 0.24].shape[0] * 2) + 1
                avg_fr = data["face_redness"].max()
                avg_fb = data["face_brightness"].max()
                print("Est blinks", est_blinks)
                if file_is_empty:
                    writer.writerow(headers) 
                write_data = [user, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "OK", str(log), max_red, avg_left, avg_right,est_blinks, exp,avg_fr,avg_fb ]
                writer.writerow(write_data)
                data_html = data.to_html()
                context = {"status": "OK", "lear":avg_left, "rear":avg_right, "er":max_red,
                             "tfc":len(data.index)*5, "eb":est_blinks, "ec":exp,"fb":avg_fb, "fr":avg_fr}

        logout(request)
        return render(request,"loginmanager/is_ok.html", context)
    else:
        return redirect("/")

def red_eyes(request):
    if request.user.is_authenticated: 
        user = request.user
        filename = "reports/" + str(user) + ".csv"
        file_exists = os.path.isfile(filename)
        log = request.session.get("log_id")
        if log:
            with open(filename, "a") as f:
                headers = ['user','date','status','log file', 'max red','left eye', 'right eye','est blinks' ,'expression', "face_redness","face_brightness"]
                file_is_empty = os.stat(filename).st_size == 0
                writer = csv.writer(f)
                data = pd.read_csv(log)
                data.columns = ["redness","face_redness","face_brightness", "left eye data", "right eye data", "expression captured"]
                avg_left = data["left eye data"].mean()
                avg_right = data["right eye data"].mean()
                exp = data["expression captured"].value_counts().index[0]
                max_red = data["redness"].max()
                est_blinks = (data[data["left eye data"] < 0.20].shape[0] * 2) + 1
                avg_fr = data["face_redness"].max()
                avg_fb = data["face_brightness"].max()
                print("Est blinks", est_blinks)
                if file_is_empty:
                    writer.writerow(headers) 
                write_data = [user, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Tired and Red", str(log), max_red, avg_left, avg_right,est_blinks, exp,avg_fr,avg_fb ]
                writer.writerow(write_data)
                data_html = data.to_html()
                context = {"status": "Tired", "lear":avg_left, "rear":avg_right, "er":max_red,
                             "tfc":len(data.index)*5, "eb":est_blinks, "ec":exp,"fb":avg_fb, "fr":avg_fr}

        logout(request)
        return render(request,"loginmanager/red_eyes.html", context)
    else:
        return redirect("/")


@csrf_exempt
def upload_image(request):
    if request.method == 'POST':
        user = request.user.username
        exp = request.POST.get("expression")
        print("Expression:",exp)
        image = request.FILES['image']
        path = "images/" + str(user) + "/"
        try:
            os.mkdir(path)
        except FileExistsError:
            pass

        # log_path = "logs/" + str(user) + "/"
        # try:
        #     os.mkdir(log_path)
        # except FileExistsError:
        #     pass
        
        # current_datetime = datetime.utcnow()
        # current_timetuple = current_datetime.utctimetuple()
        # current_timestamp = calendar.timegm(current_timetuple)
        # log_file = log_path + str(current_timestamp) + ".csv"

        # if "log_id" not in request.session:
        #     request.session["log_id"] = log_file



        eye_resp = process_eyes(request, image, path, exp)


        # if eye_resp == 3:
        #     return HttpResponse(status=203) #means the eye is drowsy
        # elif eye_resp == 1:
        #     return HttpResponse(status=201) # means its fine
        # elif eye_resp == 0:
        #     return HttpResponse(status=202) #means the eye is red
        if eye_resp == 1:
            return HttpResponse(status = 202)
        elif eye_resp == 3:
            return HttpResponse(status = 203)
        else:
            return HttpResponse(status=400)

def process_emotion(emotion_data):
    
    positive_emotions = ["happy", "neutral", "surprised"]
    negative_emotions = ["angry", "disgusted", "sad", "fearful"]
    
    if emotion_data in positive_emotions:
        return 1
    else:
        return 0
    

from scipy.spatial import distance as dist

def eye_aspect_ratio(eye):
    #Eye Aspect Ratio
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])
	C = dist.euclidean(eye[0], eye[3])
	ear = (A + B) / (2.0 * C)
	return ear

def is_eyes_closed(picture):   
    eye_cascPath = 'resources/haarcascade_eye_tree_eyeglasses.xml' 
    eyeCascade = cv2.CascadeClassifier(eye_cascPath)
    eyes = eyeCascade.detectMultiScale(
                picture,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(30, 30),
                # flags = cv2.CV_HAAR_SCALE_IMAGE
            )
    
    if len(eyes) == 0:
        return True
    
    else:
        return False


def process_eyes(request, picture, path, expression):

    if "eye_complete" in request.session:
        return 3
    
    loaded_image = face_recognition.load_image_file(picture)
    
    face_locations = face_recognition.face_locations(loaded_image)
    if len(face_locations) > 1:
        return 2
    
    if len(face_locations) == 0:
        return 2

    else:
        for top, right, bottom, left in face_locations:
            face = loaded_image[top:bottom, left:right]
        face_landmarks = face_recognition.face_landmarks(face)
        right_eye = face_landmarks[0]['right_eye']
        left_eye = face_landmarks[0]['left_eye']

        right_ear = eye_aspect_ratio(right_eye)
        left_ear = eye_aspect_ratio(left_eye)
        log_file = request.session.get("log_id")
        print("Log File", log_file)
        
        face_redness = calculate_redness(face)
        face_brightness = calculate_brightness(face)
        face_brightness = face_brightness * 10
        

        # if right_ear < 0.20 or left_ear < 0.20:
        #     if log_file is not None:
        #         with open(log_file, "a") as f:
        #             writer = csv.writer(f)
        #             data = [0,face_redness, face_brightness,left_ear, right_ear, expression]
        #             writer.writerow(data)
        #     return 3

        right_eye_mask = np.ones(face.shape, dtype=np.uint8)
        right_eye_mask.fill(255)
        left_eye_mask = np.ones(face.shape, dtype=np.uint8)
        left_eye_mask.fill(255)
        right_eye_image = cv2.fillPoly(right_eye_mask, [np.array(right_eye)], 0)
        left_eye_image = cv2.fillPoly(left_eye_mask, [np.array(left_eye)], 0)
        masked_right_eye = cv2.bitwise_or(face, right_eye_mask)
        masked_left_eye = cv2.bitwise_or(face, left_eye_mask)
        #cv2.imwrite("IMG_eye.png", masked_right_eye)
        right_eye_redness = calculate_redness(masked_right_eye)
        left_eye_redness = calculate_redness(masked_left_eye)
        total = right_eye_redness + left_eye_redness
        current_datetime = datetime.datetime.utcnow()
        current_timetuple = current_datetime.utctimetuple()
        current_timestamp = calendar.timegm(current_timetuple)
        filename = str(current_timestamp) + ".png"
        face_rgb = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
        cv2.imwrite(path + filename, face_rgb)

        log = Log(person = request.user, redness_eye=total, left_ear = left_ear, right_ear = right_ear,
                redness_face = face_redness, exposure_face = face_brightness, expression = expression)
        log.save()

        diff = timezone.now() - timezone.timedelta(seconds=60)
        print(diff)

        curr_data = Log.objects.filter(person = request.user, reported = False,
            record_time__gte=timezone.now() - timezone.timedelta(seconds=60))

        print(curr_data)

        print("Length: ",curr_data.count())
        if curr_data:
            if curr_data.count()> 12:
                request.session['eye_complete'] = True
                return 3
        else:
            return 1
        
        # if "count" not in request.session:
        #         request.session['count'] = 1
        #         return 1
        # else:
        #     request.session['count']+= 1
        #     total_count = request.session['count']
        #     print("total_count: ",total_count)
        #     if total_count > 20:
        #         return 3
        #     else:
        #         return 1



        
        
        # if log_file is not None:
        #     with open(log_file, "a") as f:
        #         writer = csv.writer(f)
        #         data = [total,face_redness,face_brightness, left_ear, right_ear, expression]
        #         writer.writerow(data)
        # print("Redness: ", total)
        # if total > 80:
        #     return 0
        # else:
        #     return 1
        
import random 

def calculate_brightness(img):
    img = exposure.equalize_adapthist(img)
    if len(img.shape) == 3:
        return np.average(norm(img, axis=2)) / np.sqrt(3)
    else:
        return np.average(img)

def calculate_redness(img):
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    mask1 = cv2.inRange(img_hsv, (0,50,20), (5,255,255))
    mask2 = cv2.inRange(img_hsv, (175,50,20), (180,255,255))
    mask_color = cv2.bitwise_or(mask1, mask2 )
    croped = cv2.bitwise_and(img, img, mask=mask_color)
    croped_rgb = cv2.cvtColor(croped, cv2.COLOR_BGR2RGB)
    
    arr_list=croped_rgb.tolist()
    r=g=b=0
    for row in arr_list:
        for item in row:
            r=r+item[0]
            g=g+item[1]
            b=b+item[2]  
    total=r+g+b
    if total == 0:
        return random.uniform(30,35)
    red=r/total*100
    green=g/total*100
    blue=b/total*100
    return red

        





def setup_face_recog(request):
    if not request.user.is_authenticated:
        return redirect("loginmanager-login")
    if request.method == 'POST':
        if "proceed" in request.POST:

            try:
                user = request.user
                user_profile = Profile.objects.get(user=user)
                context = {'profile':user_profile}
                return render(request, "loginmanager/register_face.html",context)
            except:
                return HttpResponse("PROFILE NOT SET UP! Please set up your profile FIRST!")
        
        if "upload" in request.POST:
            return redirect("upload_custom_image")

        if "delete_fr" in request.POST:
            print("Recieved FR")
            status = delete_frd(request.user)
            if status:
                messages.info(request,"Deleted Successfully")
            else:
                messages.warning(request, "FAILED to delete")
                return render(request, "loginmanager/setup_face_recog.html")
    return render(request, "loginmanager/setup_face_recog.html")

def face_recog_login(request):
    if request.user.is_authenticated:
        return redirect("/")
    else:
        return render(request, "loginmanager/face_recog_login.html")

@csrf_exempt
def register_image(request):
    if request.method == 'POST':
        user = request.user.username
        image = request.FILES['image']
        
        print(image)
        print(user)
        resp = upload_face(image,user)
        print(resp)
        if resp == 1:
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)
        
def upload_custom_image(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        user = request.user.username
        if form.is_valid():
            resp = upload_face(request.FILES['file'], user)
            if resp == 1:
                messages.success(request, "Image upload successful")
                
            else:
                messages.warning(request, "ERROR!!! Image upload FAILED. Use another image")
            return redirect("upload_custom_image")
        print(user)
    else:
        form = UploadFileForm

    return render(request,"loginmanager/upload_custom_image.html", {'form':form})





@csrf_exempt
def identify_user(request):
    print(request)
    image = request.FILES["image"]
    print(image)
    resp = recognize_face(image)
    print(resp)
    #resp = "ayush"
    if resp==None:
        return HttpResponse(status=204)
    else:
        user_id = User.objects.get(username=resp)
        login(request, user_id, backend=settings.AUTHENTICATION_BACKENDS[0])
        return HttpResponse(status=202)


def upload_face(picture, username):

    """
    Function to upload encodings of face of a person extracted from the image. It connects to Postgres DB 
    to upload the encodings to a cube. Make sure the Postgres is running and is configured properly.
    """
    try:
        connection = psycopg2.connect(user = "postgres",
                                            password = "postgres",
                                            host = "db",
                                            port = "5432")
        connection.set_session(autocommit=True)

        cursor = connection.cursor()
        print ( connection.get_dsn_parameters(),"\n")

        print("connected")
        
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)


    loaded_image = face_recognition.load_image_file(picture)
    print("loaded")
    face_locations = face_recognition.face_locations(loaded_image)

    if len(face_locations) > 1:
        return 2
    
    if len(face_locations) == 0:
        return 2

    else:
        #Align the image so that the eyes are in the X axis.
        face_landmarks = face_recognition.face_landmarks(loaded_image)
        (top, right, bottom, left) = face_locations[0]
        
        desiredWidth = (right - left)
        desiredHeight = (bottom - top)
        leftEyePts = face_landmarks[0]['left_eye']
        rightEyePts = face_landmarks[0]['right_eye']

        

        right_ear = eye_aspect_ratio(leftEyePts)
        left_ear = eye_aspect_ratio(rightEyePts)
        user_id = User.objects.get(username=username)
        profile = Profile.objects.get(user=user_id)
        if profile.lear:
            if profile.lear < left_ear:
                profile.lear = left_ear
        else:
            profile.lear =left_ear        
        if profile.rear:
            if profile.rear < right_ear:
                profile.rear = right_ear
        else:
            profile.rear = right_ear
        profile.save()

        print("REAR:",right_ear,"LEAR:",left_ear)
        if len(leftEyePts) == 0 or len(rightEyePts) == 0:
            return(2, "Image not suitable for registration. Take another")

        else:
            leftEyeCenter = np.array(leftEyePts).mean(axis=0).astype("int")
            rightEyeCenter = np.array(rightEyePts).mean(axis=0).astype("int")
            leftEyeCenter = (leftEyeCenter[0],leftEyeCenter[1])
            rightEyeCenter = (rightEyeCenter[0],rightEyeCenter[1])
            #cv2.circle(image, leftEyeCenter, 2, (255, 0, 0), 10)
            #cv2.circle(image, rightEyeCenter, 2, (255, 0, 0), 10)
            dY = rightEyeCenter[1] - leftEyeCenter[1]
            dX = rightEyeCenter[0] - leftEyeCenter[0]
            angle = np.degrees(np.arctan2(dY, dX))
            desiredLeftEye=(0.35, 0.35)
            desiredFaceWidth = desiredWidth
            desiredFaceHeight = desiredHeight
            desiredRightEyeX = 1.0 - desiredLeftEye[0]
            dist = np.sqrt((dX ** 2) + (dY ** 2))
            desiredDist = (desiredRightEyeX - desiredLeftEye[0])
            desiredDist *= desiredFaceWidth
            scale = desiredDist / dist 
            eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2,
                (leftEyeCenter[1] + rightEyeCenter[1]) // 2)
            M = cv2.getRotationMatrix2D(eyesCenter, angle, scale)
            tX = desiredFaceWidth * 0.5
            tY = desiredFaceHeight * desiredLeftEye[1]
            M[0, 2] += (tX - eyesCenter[0])
            M[1, 2] += (tY - eyesCenter[1])         
            (w, h) = (desiredFaceWidth, desiredFaceHeight)
            #(y2,x2,y1,x1) = face_locations[0] 
            output = cv2.warpAffine(loaded_image, M, (w, h),flags=cv2.INTER_CUBIC)
            output = cv2.cvtColor(output, cv2.COLOR_BGR2RGB)
            aligned_image = cv2.resize(output,(128,128))

        current_encoding = face_recognition.face_encodings(aligned_image)
        print(current_encoding[0][64:128])
        print(username)
        #Upload the encodings to the postgres server
        try:
            query = "INSERT INTO vectors (username, vec_low, vec_high) VALUES ('{}', CUBE(array[{}]), CUBE(array[{}]))".format(
            username,
            ','.join(str(s) for s in current_encoding[0][0:64]),
            ','.join(str(s) for s in current_encoding[0][64:128]),
        )
            print(query)
            cursor.execute(query)
            print("done")

        except (Exception, psycopg2.Error) as error :
            print ("Error while uploading to PostgreSQL", error)
            return 2

        return 1




def recognize_face(picture):

    try:
        connection = psycopg2.connect(user = "postgres",
                                            password = "postgres",
                                            host = "db",
                                            port = "5432")
        connection.set_session(autocommit=True)

        cursor = connection.cursor()
        
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

    loaded_image = face_recognition.load_image_file(picture)
    current_encodings = face_recognition.face_encodings(loaded_image)
    print(len(current_encodings))
    if len(current_encodings) == 0:
        return None
    
    threshold = 0.5
    try:
        row = find_match(current_encodings[0], threshold, cursor)
        

        if len(row)==0:
            return None
        return row[0][0]

    except (Exception, psycopg2.Error) as error :
        print ("Error while uploading to PostgreSQL", error)

def find_match(encodings, threshold, cursor):
    print(len(encodings))
    
    query = "SELECT username FROM vectors WHERE (sqrt(power(CUBE(array[{}]) <-> vec_low, 2) + power(CUBE(array[{}]) <-> vec_high, 2))) <= {} ".format(
        ','.join(str(s) for s in encodings[0:64]),
        ','.join(str(s) for s in encodings[64:128]),
        threshold,
    ) + \
        "ORDER BY sqrt((CUBE(array[{}]) <-> vec_low) + (CUBE(array[{}]) <-> vec_high)) ASC LIMIT 1".format(
        ','.join(str(s) for s in encodings[0:64]),
        ','.join(str(s) for s in encodings[64:128]),
    )
    print(query)
    cursor.execute(query)
    print("query exec")
    row = cursor.fetchall()
    print(row)
    return row


def del_face_recog_data(request):
    user = request.user
    delete_status = delete_frd(user)
    print(delete_status)
    if delete_status:
        return render(request, "loginmanager/face_recog_delete_success.html")
    else:
        return render(request, "loginmanager/face_recog_delete_failed.html")


def delete_frd(user):
    del_query = "DELETE FROM vectors WHERE username = '{}'".format(user)
    print(del_query)
    try:
        connection = psycopg2.connect(user = "postgres",
                                            password = "postgres",
                                            host = "db",
                                            port = "5432")
        connection.set_session(autocommit=True)

        cursor = connection.cursor()
    
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
        return 0

    try:
        cursor.execute(del_query)
        return 1
    except:
        return 0