from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _



# Create your models here.
gender_choice = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
)

class Profile(models.Model):
    user = models.OneToOneField(User, help_text=_('User'), on_delete=models.CASCADE, null=True)
    first_name = models.CharField(help_text=_('first name'), max_length=200, null=True, blank=True)
    last_name = models.CharField(help_text=_('last name'), max_length=200, null=True, blank=True)
    email = models.CharField(help_text=_('Email'), max_length=200, null=True, blank=True)
    gender = models.CharField(help_text=_('Gender'), max_length=10, choices=gender_choice, null=True, blank=True)
    lear = models.FloatField(help_text = _('Left Eye Data'),blank=True, null=True)
    rear = models.FloatField(help_text=_("Right Eye Data"), blank=True, null=True)
    
    

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profile')

    def __str__(self):
        return self.first_name

class Log(models.Model):
    person = models.ForeignKey(User, help_text=_('Profile'), on_delete = models.CASCADE, null=False)
    redness_eye = models.FloatField(help_text=_("Eye Redness"), blank = True, null=True)
    left_ear = models.FloatField(help_text=_("Left Eye Data"), blank = True, null = True)
    right_ear = models.FloatField(help_text = _("Right Eye Data"), blank=True, null=True)
    redness_face = models.FloatField(help_text = _("Face Redness"), blank = True, null = True)
    exposure_face = models.FloatField(help_text = _("Face Exposure"), blank = True, null = True)
    expression = models.CharField(help_text = _("Face Expression"), max_length = 30, blank = True, null = True)
    record_time = models.DateTimeField(help_text = _("Time Period"), default = timezone.now, null = True, blank = True)
    reported = models.BooleanField(help_text = "True if the data is processed", default = False)

    class Meta:
        verbose_name = _('Log')
        verbose_name_plural = _('Logs')

    def __str__(self):
        return str('%s %s' % (self.record_time ,self.person))

class Report(models.Model):
    person = models.ForeignKey(User, help_text=_('Profile'), on_delete = models.CASCADE, null=False)
    final_pred = models.CharField(help_text=_('Final Prediction'),max_length = 30, blank=False, null=False)
    lear_avg = models.FloatField(help_text=_("Left Eye Data"), blank=False, null=False)
    rear_avg = models.FloatField(help_text=_("Right Eye Data"), blank=False, null=False)
    lblinks = models.IntegerField(help_text=_("Left Eye blinks"), blank = False, default = 0)
    rblinks = models.IntegerField(help_text=_("Right Eye blinks"), blank = False, default = 0)
    eye_redness_max = models.FloatField(help_text=_("Eye Redness"), blank = False, null=False)
    face_exposure_avg = models.FloatField(help_text=_("Face Exposure"), blank=False, null=False)
    face_redness_avg = models.FloatField(help_text=_("Face Redness"), blank = False, null=False)
    expression = models.CharField(help_text=_("Face Expression"),max_length = 30, blank=False, null=False)
    record_time = models.DateTimeField(help_text = _("Time Period"), default = timezone.now, null = True, blank = True)

    class Meta:
        verbose_name = _('Report')
        verbose_name_plural = _('Reports')

    def __str__(self):
        return str('%s %s' % (self.record_time ,self.person))