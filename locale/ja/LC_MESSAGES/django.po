# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-10 19:13+0545\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: EmployeeTimeTracker/settings.py:159
msgid "Administration"
msgstr "サイト管理"

#: EmployeeTimeTracker/urls.py:14
msgid "admin/"
msgstr ""

#: home/templates/home/index.html:6 home/templates/home/main.html:71
msgid "Dashboard"
msgstr "トップ画面"

#: home/templates/home/index.html:15 loginmanager/models.py:24
msgid "Monthly Pay"
msgstr "毎月の支払額"

#: home/templates/home/index.html:24 loginmanager/models.py:18
#: loginmanager/templates/loginmanager/all_users.html:20
msgid "Role"
msgstr "役職"

#: home/templates/home/index.html:33
msgid "Phone Number"
msgstr "電話番号"

#: home/templates/home/index.html:42 loginmanager/models.py:23
msgid "Rate Per Hour"
msgstr "時給"

#: home/templates/home/index.html:55
msgid "Area Chart Example"
msgstr "エリアチャート"

#: home/templates/home/index.html:64
msgid "Bar Chart Example"
msgstr "バーチチャート"

#: home/templates/home/index.html:72
msgid "Please Create or Update profile from settings."
msgstr "設定からプロファイルを作成または更新してください"

#: home/templates/home/index.html:72 home/templates/home/main.html:53
#: home/templates/home/main.html:87
msgid "Settings"
msgstr "設定"

#: home/templates/home/main.html:56
msgid "Logout"
msgstr "ログアウト"

#: home/templates/home/main.html:68
msgid "Menu"
msgstr "メニュー"

#: home/templates/home/main.html:75
msgid "Check IN/OUT"
msgstr "出勤/退勤"

#: home/templates/home/main.html:79 message/models.py:15 message/models.py:16
#: message/templates/message/index.html:6
#: message/templates/message/index.html:11
#: message/templates/message/list_all_messages.html:18
#: timecard/templates/timecard/index.html:15
#: timecard/templates/timecard/index.html:26
msgid "Message"
msgstr "メッセージ"

#: home/templates/home/main.html:83
#: timecard/templates/timecard/allrecord.html:6
#: timecard/templates/timecard/record.html:6
msgid "Record"
msgstr "勤務記録"

#: home/templates/home/main.html:91
msgid "Manage"
msgstr "管理する"

#: home/templates/home/main.html:94
msgid "All Staff Records"
msgstr "全社員記録"

#: home/templates/home/main.html:99
#, fuzzy
#| msgid "Message"
msgid "Messages"
msgstr "メッセージ"

#: home/templates/home/main.html:104
#, fuzzy
#| msgid "User"
msgid "Users"
msgstr "ユーザー名"

#: home/templates/home/main.html:109
msgid "Company"
msgstr "会社"

#: home/templates/home/main.html:173
msgid "Logged in as:"
msgstr "ログイン中"

#: home/templates/home/user.html:3
msgid "Welcome to User page."
msgstr ""

#: home/views.py:24
msgid "Hello"
msgstr ""

#: loginmanager/forms.py:18
msgid "This email is already used"
msgstr "本メールアドレスはすでに登録済みです。"

#: loginmanager/models.py:13 message/models.py:10
#: message/templates/message/acknowledge_message.html:13
#: message/templates/message/list_all_messages.html:17 timecard/models.py:13
msgid "User"
msgstr "ユーザー名"

#: loginmanager/models.py:14
#: loginmanager/templates/loginmanager/profile.html:22 test/views.py:8
msgid "first name"
msgstr "名"

#: loginmanager/models.py:15
#: loginmanager/templates/loginmanager/profile.html:26
msgid "last name"
msgstr "性"

#: loginmanager/models.py:16
#: loginmanager/templates/loginmanager/all_users.html:18
#: loginmanager/templates/loginmanager/profile.html:30
msgid "Email"
msgstr "メールアドレス"

#: loginmanager/models.py:17
#: loginmanager/templates/loginmanager/all_users.html:19
#: loginmanager/templates/loginmanager/company_details.html:67
#: loginmanager/templates/loginmanager/profile.html:34
msgid "Phone"
msgstr "電話番号"

#: loginmanager/models.py:19
#: loginmanager/templates/loginmanager/profile.html:38
msgid "Gender"
msgstr "性別"

#: loginmanager/models.py:20
#: loginmanager/templates/loginmanager/profile.html:42
msgid "Address"
msgstr "住所"

#: loginmanager/models.py:21
#: loginmanager/templates/loginmanager/profile.html:46
msgid "Bank name"
msgstr "銀行名"

#: loginmanager/models.py:22
#: loginmanager/templates/loginmanager/profile.html:50
msgid "Bank account"
msgstr "銀行口座"

#: loginmanager/models.py:28 loginmanager/models.py:29
#: loginmanager/templates/loginmanager/profile.html:11
msgid "Profile"
msgstr "プロフィール"

#: loginmanager/templates/loginmanager/all_users.html:7
#, fuzzy
#| msgid "User"
msgid "All Users"
msgstr "ユーザー名"

#: loginmanager/templates/loginmanager/all_users.html:16
#: loginmanager/templates/loginmanager/company_details.html:28
msgid "Name"
msgstr "名前"

#: loginmanager/templates/loginmanager/all_users.html:17
#: loginmanager/templates/loginmanager/login.html:46
#: timecard/templates/timecard/allrecord.html:20
#: timecard/templates/timecard/report.html:19
#: timecard/templates/timecard/report.html:30 timecard/views.py:126
msgid "Username"
msgstr "ユーザー名"

#: loginmanager/templates/loginmanager/all_users.html:21
#: message/templates/message/list_all_messages.html:20
msgid "Actions"
msgstr "行動"

#: loginmanager/templates/loginmanager/all_users.html:48
msgid "See All Details"
msgstr "全ての情報を見る"

#: loginmanager/templates/loginmanager/all_users.html:49
#: timecard/templates/timecard/staff_record.html:14
#, fuzzy
#| msgid "Reports"
msgid "Report"
msgstr "レポート"

#: loginmanager/templates/loginmanager/company_details.html:12
msgid "Company Details"
msgstr "会社情報"

#: loginmanager/templates/loginmanager/company_details.html:16
msgid "Edit Company Details"
msgstr "会社情報編集"

#: loginmanager/templates/loginmanager/company_details.html:36
msgid "Company address"
msgstr "会社住所"

#: loginmanager/templates/loginmanager/company_details.html:44
msgid "Company email"
msgstr "会社メール"


msgid "Logo"
msgstr "ロゴ"

#: loginmanager/templates/loginmanager/company_details.html:85
msgid "Cancel"
msgstr "キャンセル"

#: loginmanager/templates/loginmanager/company_details.html:86
msgid "Save Changes"
msgstr "変更内容保存"

#: loginmanager/templates/loginmanager/company_not_found.html:1
msgid "Company profile not setup. Please contact the provider."
msgstr "会社のプロフィールが設定されていません。 プロバイダーにお問い合わせください"

#: loginmanager/templates/loginmanager/delete_employee_confirm.html:12
#: loginmanager/templates/loginmanager/profile_details.html:12
msgid "Employee Details"
msgstr "従業員の詳細"

#: loginmanager/templates/loginmanager/delete_employee_confirm.html:17
msgid "Employee"
msgstr "従業員"

#: loginmanager/templates/loginmanager/delete_employee_confirm.html:17
msgid "user account and profile would be deleted. Are you sure?"
msgstr "に関連付けられているユーザーアカウントを削除してもよろしいですか？"

#: loginmanager/templates/loginmanager/delete_employee_confirm.html:28
msgid "Yes, Delete it"
msgstr "はい、削除"

#: loginmanager/templates/loginmanager/delete_employee_confirm.html:40
msgid "No"
msgstr "いいえ"

#: loginmanager/templates/loginmanager/login.html:40
#: loginmanager/templates/loginmanager/login.html:63
#: loginmanager/templates/loginmanager/password_reset.html:46
#: loginmanager/templates/loginmanager/password_reset_complete.html:33
#: loginmanager/templates/loginmanager/password_reset_confirm.html:47
#: loginmanager/templates/loginmanager/password_reset_done.html:32
#: loginmanager/templates/loginmanager/register.html:48
msgid "Login"
msgstr "ログイン"

#: loginmanager/templates/loginmanager/login.html:50
msgid "Password"
msgstr "パスワード入力"

#: loginmanager/templates/loginmanager/login.html:56
msgid "Remember Password"
msgstr "パスワードを保存"

#: loginmanager/templates/loginmanager/login.html:64
msgid "Forgot password?"
msgstr "パスワードを忘れた場合"

#: loginmanager/templates/loginmanager/login.html:69
msgid "Need an account? Sign up!"
msgstr "アカウントを作成する"

#: loginmanager/templates/loginmanager/password_reset.html:27
#: loginmanager/templates/loginmanager/register.html:26
#: loginmanager/templates/loginmanager/register.html:44
msgid "アカウント作成"
msgstr ""

#: loginmanager/templates/loginmanager/password_reset.html:37
#: loginmanager/templates/loginmanager/password_reset_confirm.html:32
#: loginmanager/templates/loginmanager/password_reset_confirm.html:43
msgid "Reset Password"
msgstr "パスワードを保存"

#: loginmanager/templates/loginmanager/password_reset_complete.html:31
msgid "Your password has been reset."
msgstr "パスワードは正常に再設定されました"

#: loginmanager/templates/loginmanager/password_reset_confirm.html:35
#: loginmanager/templates/loginmanager/register.html:38
msgid "Type Password"
msgstr "パスワード入力"

#: loginmanager/templates/loginmanager/password_reset_confirm.html:37
#: loginmanager/templates/loginmanager/register.html:40
msgid "Password again"
msgstr "パスワード再入力"

#: loginmanager/templates/loginmanager/password_reset_done.html:30
msgid "An email has been sent with instruction to reset your password."
msgstr "パスワードの再設定に関するメールを返信しました"

#: loginmanager/templates/loginmanager/profile.html:54


msgid "Profile Pic"
msgstr "プロフィール画像"

#: loginmanager/templates/loginmanager/profile.html:68
msgid "Update Information"
msgstr "更新情報"

#: loginmanager/templates/loginmanager/profile_details.html:18
msgid "Delete Employee"
msgstr "従業員を削除"

#: loginmanager/templates/loginmanager/register.html:33
msgid "Your Username"
msgstr "ユーザー名"

#: loginmanager/templates/loginmanager/register.html:36
msgid "Your Email A/C"
msgstr "Eメール"

#: message/filters.py:9 timecard/filters.py:9
#: timecard/templates/timecard/record.html:18
msgid "Select Date"
msgstr "日付を選択"

#: message/models.py:8
msgid "message"
msgstr "メッセージ"

#: message/models.py:9 message/templates/message/list_all_messages.html:16
msgid "Date Created"
msgstr "作成日"

#: message/models.py:11 message/templates/message/list_all_messages.html:19
msgid "Acknowledge"
msgstr "認める"

#: message/templates/message/acknowledge_message.html:7
#: message/templates/message/list_all_messages.html:50

msgid "Acknowledge Message"
msgstr "認める"

#: message/templates/message/acknowledge_message.html:13
msgid "says"
msgstr "による"

#: message/templates/message/acknowledge_message.html:23
msgid "Reply"
msgstr "応答"

#: message/templates/message/acknowledge_message.html:26
#: message/templates/message/index.html:15
msgid "Send"
msgstr "送る"

#: message/templates/message/list_all_messages.html:7
#, fuzzy
#| msgid "Message"
msgid "All Message"
msgstr "メッセージ"


msgid "Message Acknowledged"
msgstr "確認済みメッセージ"

#: test/templates/test/index.html:3
msgid "This is Test File"
msgstr ""

#: timecard/filters.py:11
msgid "Search"
msgstr "検索"

msgid "Search User"
msgstr "検索 ユーザー"

#: timecard/models.py:8
msgid "CheckIn time"
msgstr "出勤 時間"

#: timecard/models.py:9 timecard/templates/timecard/allrecord.html:22
#: timecard/templates/timecard/staff_record.html:24
msgid "CheckIn Message"
msgstr "出勤 メッセージ"

#: timecard/models.py:10
msgid "CheckOut time"
msgstr "退勤 時間"

#: timecard/models.py:11 timecard/templates/timecard/allrecord.html:24
#: timecard/templates/timecard/staff_record.html:26 timecard/views.py:126
msgid "CheckOut Message"
msgstr "退勤 メッセージ"

#: timecard/models.py:12
msgid "status"
msgstr "状態"

#: timecard/models.py:16
msgid "Daily Log"
msgstr "毎日のログ"

#: timecard/models.py:17
msgid "Daily Logs"
msgstr "毎日のログ"

#: timecard/models.py:23
msgid "name"
msgstr "名前"

#: timecard/models.py:24
msgid "Time Period"
msgstr "期間"

#: timecard/models.py:27 timecard/models.py:28
msgid "Break Time"
msgstr "出勤 時間"

#: timecard/templates/timecard/allrecord.html:12
msgid "All Staffs Daily Report"
msgstr "全社員記録"

#: timecard/templates/timecard/allrecord.html:21
#: timecard/templates/timecard/record.html:29
#: timecard/templates/timecard/report.html:20
#: timecard/templates/timecard/report.html:31
#: timecard/templates/timecard/staff_record.html:23 timecard/views.py:126
msgid "CheckIn Time"
msgstr "出勤 時間"

#: timecard/templates/timecard/allrecord.html:23
#: timecard/templates/timecard/record.html:30
#: timecard/templates/timecard/report.html:22
#: timecard/templates/timecard/report.html:33
#: timecard/templates/timecard/staff_record.html:25 timecard/views.py:126
msgid "CheckOut Time"
msgstr "退勤 時間"

#: timecard/templates/timecard/allrecord.html:25
#: timecard/templates/timecard/record.html:31
#: timecard/templates/timecard/report.html:24
#: timecard/templates/timecard/report.html:35
#: timecard/templates/timecard/staff_record.html:27 timecard/views.py:126
msgid "Duration"
msgstr "合計時間"

#: timecard/templates/timecard/index.html:6
msgid "Time Register"
msgstr "時間登録"

#: timecard/templates/timecard/index.html:12
msgid "Punch In"
msgstr "出勤"

#: timecard/templates/timecard/index.html:23
msgid "You have checked In at"
msgstr "＊＊時刻に入場しました。"

#: timecard/templates/timecard/index.html:24
msgid "Punch Out"
msgstr "退勤"

#: timecard/templates/timecard/record.html:16
msgid "Your Daily Login Reports"
msgstr "毎日のログインレポート"



#: timecard/templates/timecard/report.html:6
#: timecard/templates/timecard/staff_record.html:6
msgid "Reports"
msgstr "レポート"


#: timecard/templates/timecard/staff_record.html:14
msgid "Download"
msgstr "ダウンロード"

#: timecard/views.py:126
msgid "CheckIN Message"
msgstr "メッセージ"

#: timecard/views.py:126
msgid "Payment"
msgstr "お支払い"

#: timecard/views.py:151
msgid "Total"
msgstr "合計"

msgid "Interface"
msgstr "インタフェス"

msgid "GetReport"
msgstr "レポート"

msgid "Create Account"
msgstr "アカウント作成"

msgid "Your username, in case you’ve forgotten:"
msgstr "ユーザ名、もし忘れた場合"

msgid "Pay"
msgstr "工賃"

msgid "per hour"
msgstr "一時間当たり"

msgid "per month"
msgstr "一月当たり"

msgid "Edit Employee Profile"
msgstr "従業員のプロフィールを編集"

msgid "Edit"
msgstr "編集"

msgid "Send Email"
msgstr "メール返信"

msgid "Checkin/Checkout with Face"
msgstr "顔で出勤 / 退勤"

msgid "Proceed"
msgstr "進む"

msgid "Setup Face Recognition"
msgstr "顔認識セットアップ"

msgid "Shift and Availability"
msgstr "シフトと希望"

msgid "Shift"
msgstr "シフト"

msgid "Availability"
msgstr "希望"

msgid "You have been assigned the following shifts"
msgstr "下記のシフトに割り当てられている。"

msgid "Shift Status"
msgstr "シフト状況"

msgid "Assigned"
msgstr "割り当てられた"

msgid "Not Assigned"
msgstr "割り当てられていない"

msgid "Shift Start"
msgstr "シフト開始"

msgid "Shift End"
msgstr "シフト終了"

msgid "Shift Duration"
msgstr "シフト期間"

msgid "Click on a date to set availability for the day"
msgstr "日付をクリックして、空き状況を設定する"

msgid "Add availability"
msgstr "希望日程を追加"

msgid "Available from"
msgstr "開始"

msgid "Available to"
msgstr "終了"

msgid "Unavailable"
msgstr "なし"

msgid "Available to should be greater than Available from."
msgstr "開始と終了時刻を確認してください。"

msgid "Proceed to Register"
msgstr "登録に進む"

msgid "Face Recognition needs access to your camera"
msgstr "顔認識にはカメラのアクセスが必要です"

msgid "Delete Face Recognition Data"
msgstr "顔認識データを削除"

msgid "If face recognition is not working for you, you can delete the data and start over"
msgstr "顔認識がスムーズに作動しない場合にデータを削除し、やりのしてください。"

msgid "Deleting face recognition data would delete all your face data from our database. "
msgstr "顔認識データを削除すると、データベースからすべての顔データが削除されます。"

msgid "You'd have to set up your face recognition again if you want to use this feature."
msgstr "本機能の使用にあたり再度顔認識をセットする必要があります。"

msgid "Do you want to delete your face recognition data?"
msgstr "顔認識データを削除しますか？"

msgid "Set Shift"
msgstr "シフト設定"

msgid "Add shift"
msgstr "シフト追加"

msgid "Available day"
msgstr "空き日"

msgid "Shift starts too early"
msgstr "シフト開始時刻より早いです。"

msgid "Shift ends too late"
msgstr "シフト終了時刻より遅いです。"

msgid "Shift set successfully"
msgstr "シフト設定を成功しました"

msgid "Do you want to send an email about this change to the user?"
msgstr "この変更についてユーザーにメールを送信しますか？"

msgid "Notify all"
msgstr "すべてに通知"

msgid "This would send an email to users that have their shift assigned. "
msgstr "シフトが割り当てられているユーザーにメールが送信されます。"

msgid "Do you want to send an email about this change to all the employees?"
msgstr "変更についてすべての従業員にメールを送信しますか？"

msgid "Face Recognition"
msgstr "顔認識"

msgid "View All Employees Shift and Availability"
msgstr "すべての従業員の希望とシフトを表示"

msgid "Eye & Face Test"
msgstr "体調判断"

msgid "Upload Image"
msgstr "画像をアップロード"

msgid "Images not meeting criteria would be rejected"
msgstr "この画像は登録できませんでした"

msgid "Face Expression"
msgstr "顔の表情"

msgid "Face Exposure"
msgstr "顔色の明暗度"

msgid "Eye Redness"
msgstr "目の赤み"

msgid "Left Eye Data"
msgstr "左目データ"

msgid "Right Eye Data"
msgstr "右目データ"

msgid "Status"
msgstr "状態"

msgid "Frames Calculated"
msgstr "計算されたフレーム"

msgid "Estimated Left Blinks"
msgstr "左目瞬き"

msgid "Estimated Right Blinks"
msgstr "右目瞬き"

msgid "Face Redness"
msgstr "顔の赤み"

msgid "Fine"
msgstr "良好"

msgid "Drowsy"
msgstr "眠気"

msgid "Tired and Sick"
msgstr "病気/疲れた"

msgid "Tired and Sleepy"
msgstr "疲れた/眠気"

msgid "Sad"
msgstr "悲しい"

msgid "Identified as"
msgstr "認識されたユーザー"

